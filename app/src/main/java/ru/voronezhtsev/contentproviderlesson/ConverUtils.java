package ru.voronezhtsev.contentproviderlesson;

import android.content.ContentValues;
import ru.voronezhtsev.contentproviderlesson.data.Note;

public class ConverUtils {
    private static final String ID = "id";
    private static final String NOTE = "note";

    public static Note convertToNote(ContentValues values) {
        return new Note(values.getAsInteger(ID), values.getAsString(NOTE));
    }
}
