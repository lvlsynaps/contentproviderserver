package ru.voronezhtsev.contentproviderlesson;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String URI = "content://" + NotesContentProvider.AUTHOTITY;
    private static final String ADD_URI_LAST_PATH_SEGMENT = "add";
    private static final String UPDATE_URI_LAST_PATH_SEGMENT = "update";
    private static final String NOTES_LIST_LAST_PATH_SEGMENT = "notes";
    private static final String CREATE_SUCCESS_LAST_PATH_SEGMENT = "success";
    private static final String CREATE_FAIL_LAST_PATH_SEGMENT = "fail";

    private TextView mNotifyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNotifyTextView = findViewById(R.id.notify_text_view);
        ContentObserver notesContentObserver = new NotesContentObserver(
                new Handler(Looper.getMainLooper()));
        getContentResolver().registerContentObserver(Uri.parse(URI), true,
                notesContentObserver);
    }

    class NotesContentObserver extends ContentObserver {

        public NotesContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            if(uri.getLastPathSegment() == null) {
                mNotifyTextView.setText(R.string.notify_change_error);
            } else {
                if (isNoteId(uri.getLastPathSegment())) {
                    if (uri.getPath().contains(UPDATE_URI_LAST_PATH_SEGMENT)) {
                        mNotifyTextView.setText(getString(R.string.update_note));
                    } else {
                        mNotifyTextView.setText(getString(R.string.note_get));
                    }
                } else if (uri.getLastPathSegment().equals(NOTES_LIST_LAST_PATH_SEGMENT)) {
                    mNotifyTextView.setText(R.string.notes_list);
                } else if (uri.getLastPathSegment().equals(ADD_URI_LAST_PATH_SEGMENT)) {
                    mNotifyTextView.setText(R.string.note_added);
                } else if(uri.getLastPathSegment().equals(CREATE_SUCCESS_LAST_PATH_SEGMENT)) {
                    mNotifyTextView.setText(R.string.create_success);
                } else if(uri.getLastPathSegment().equals(CREATE_FAIL_LAST_PATH_SEGMENT)) {
                    mNotifyTextView.setText(R.string.create_fail);
                } else {
                    mNotifyTextView.setText(R.string.unknown_uri);
                }
            }
        }
        private boolean isNoteId(String lastPathSegment) {
            return lastPathSegment.matches("\\d+");
        }
    }

}
